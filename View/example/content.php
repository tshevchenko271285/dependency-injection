<div class="container pt-5 mt-5">

    <form method="POST">
        <input type="hidden" name="test" value="test-value">
        <button type="submit" class="btn btn-primary">Go to post method</button>
    </form>

    <hr>

    <h2><?php echo $title; ?></h2>
    <h2><?php var_dump(\Tim\Core\Config::get('test.data.inner2', 'default')); ?></h2>

    <p><?php echo $desc; ?></p>

    <code><?php echo __FILE__; ?></code>

    <hr>
</div>
<div class="container">
    <table class="table d-block" style="max-width: 100%; font-size: 13px;">
        <thead>
        <tr>
            <th>key</th>
            <th>rawData</th>
            <th>pattern</th>
            <th>controller</th>
            <th>method</th>
            <th>params</th>
        </tr>
        </thead>
        <tbody>
        <?php if($routes) : ?>
            <?php foreach ($routes as $route) : ?>
                <tr>
                    <td><?php echo($route->key); ?></td>
                    <td><?php echo($route->rawData); ?></td>
                    <td><?php echo($route->pattern); ?></td>
                    <td><?php echo($route->controller); ?></td>
                    <td><?php echo($route->method); ?></td>
                    <td><?php var_dump($route->params); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>


        </tbody>
    </table>

</div>
