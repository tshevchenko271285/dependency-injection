<?php
namespace Tim\Core\Contracts;

interface Singleton
{
    public static function getInstance(): self;
}