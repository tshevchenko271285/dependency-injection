<?php
namespace Tim\Core;

class View
{
    /**
     * VIEW_PATH
     */
    const VIEW_PATH = __DIR__ . '/../View/';

    /**
     * FILE_EXTENSION
     */
    const FILE_EXTENSION = '.php';

    /**
     * @param string $viewName
     * @param array $data
     * @return string
     */
    public static function render(string $viewName, array $data = []): string
    {
        $viewPath = str_replace('.','/', $viewName);
        $path = self::VIEW_PATH . $viewPath . self::FILE_EXTENSION;

        if (!file_exists($path)) {
            throw new \RuntimeException ('View ' . $path . ' does not exist');
        }

        extract($data);

        ob_start();
        require $path;
        return ob_get_clean();
    }

}