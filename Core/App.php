<?php

namespace Tim\Core;

use Tim\Core\Contracts\Controller;
use Tim\Core\Contracts\Singleton;

class App implements Singleton
{
    /**
     * CONSTRUCTOR NAME
     */
    const CONSTRUCTOR_NAME = '__construct';

    /**
     *  Singleton Contract
     */
    const SINGLETON_CONTRACT = 'Tim\Core\Contracts\Singleton';

    /**
     * Singleton Get Instance Method
     */
    const SINGLETON_METHOD = 'getInstance';

    /**
     * CONTROLLER_NAMESPACE
     */
    const CONTROLLER_NAMESPACE = 'Tim\App\Controllers\\';

    /**
     * @var $this |App|null
     */
    protected static ?self $instance = null;

    /**
     * @var array
     */
    protected array $container = [];

    /**
     * @var Request
     */
    public Request $request;

    /**
     * @var array
     */
    public array $routes = [];

    /**
     * @var Route
     */
    public ?Route $route;

    public Controller $currentController;

    public \ReflectionMethod $currentControllerMethod;

    /**
     * App constructor.
     */
    protected function __construct()
    {
        self::$instance = $this;

        $this->container = Config::get('container', []);

        $this->request = Request::getInstance();

        foreach (Config::get('routes', []) as $key => $route) {
            $this->routes[$key] = $this->makeRoute($key, $route);
        }
    }

    /**
     * @return static
     */
    public static function getInstance() : self
    {
        return self::$instance ?? new self;
    }

    public function run()
    {
        try {
            $className = self::CONTROLLER_NAMESPACE . $this->route->controller;
            $reflectionClass = new \ReflectionClass($className);
            // Make Controller Object
            $this->currentController = self::makeObject($className);
            // Get Method
            $this->currentControllerMethod = $reflectionClass->getMethod($this->route->method);
            // Make Params for method
            $params = self::makeParams($this->currentControllerMethod);

            $this->currentControllerMethod->invokeArgs($this->currentController, $params);
        } catch (\Throwable $e) {
            throw new \RuntimeException($e);
        }
    }

    /**
     * @param \ReflectionMethod $method
     * @return array
     * @throws \ReflectionException
     */
    public function makeParams(\ReflectionMethod $method): array
    {
        $result = [];
        $params = $method->getParameters();
        foreach ($params as $param) {
            if ($param->hasType()) {
                $result[] = $this->makeObject($param->getType()->getName());
            }
        }
        return $result;
    }

    /**
     * @param $className
     * @return mixed|object
     */
    public function makeObject($className)
    {
        try {
            if ($this->hasRelation($className)) {
                $className = $this->getRelation($className);
            }
            $reflectionClass = new \ReflectionClass($className);

            // If singleton
            if (
                $reflectionClass->implementsInterface(self::SINGLETON_CONTRACT)
                && $reflectionClass->hasMethod(self::SINGLETON_METHOD)
            ) {
                return $reflectionClass->getMethod(self::SINGLETON_METHOD)->invoke(null);
            }

            $params = [];
            if ($reflectionClass->hasMethod(self::CONSTRUCTOR_NAME)) {
                $constructor = $reflectionClass->getMethod(self::CONSTRUCTOR_NAME);
                if ($constructor->isPublic()) {
                    $params = self::makeParams($constructor);
                }
            }
            // Create Controller object
            $controllerObject = $reflectionClass->newInstanceArgs($params);
            return $controllerObject;
        } catch (\Throwable $e) {
            throw new \RuntimeException($e);
        }
    }

    /**
     * @param $key
     * @param $route
     * @return Route
     */
    protected function makeRoute($key, $route)
    {
        $routeObj = new Route($key, $route);
        if (preg_match($routeObj->pattern, $this->request->uri)) {
            $routeObj->setParams($this->request->method, $this->request->uri);
            $this->route = $routeObj;
        }
        return $routeObj;
    }

    /**
     * @param $contract
     * @return bool
     */
    public function hasRelation(string $contract): bool
    {
        return isset($this->container[$contract]);
    }

    /**
     * @param string $contract
     * @return string|null
     */
    public function getRelation(string $contract): ?string
    {
        if ($this->hasRelation($contract)) {
            return $this->container[$contract];
        } else {
            return null;
        }
    }
}