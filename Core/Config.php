<?php
namespace Tim\Core;

class Config
{
    const CONFIG_PATH = __DIR__ . '/../Config/';

    /**
     * @param string $path
     * @param mixed $default
     * @return mixed
     */
    public static function get(string $path, $default = null)
    {
        $pathArray = explode('.', $path);
        $fileName = array_shift($pathArray);

        $data = include self::CONFIG_PATH . $fileName.'.php';

        while (is_array($data) && count($pathArray) && isset($data[$pathArray[0]])) {
            $data = $data[array_shift($pathArray)];
        }
        // If there is an unused path set default value
        if (count($pathArray)) {
            $data = $default;
        }
        return $data;
    }
}