<?php

namespace Tim\Core;

use \Tim\Core\Contracts\Route as BaseRoute;

class Route implements BaseRoute
{
    const DELIMITER_ROUTER = '@';

    const PARAM_PATTERN = '/\{\w+\}/';

    const PARAM_REPLACEMENT_PATTER = '[\w-]+';

    /**
     * @var string
     */
    protected string $key = '';

    /**
     * @var string
     */
    protected string $rawData = '';

    /**
     * @var string
     */
    protected string $pattern = '';

    /**
     * @var string
     */
    protected string $controller = '';

    /**
     * @var string
     */
    protected string $method = '';

    /**
     * @var array
     */
    protected array $params = [];

    /**
     * Route constructor.
     * @param $key
     * @param $value
     */
    public function __construct(string $key, string $value)
    {
        // Make pattern
        $pattern = preg_replace(self::PARAM_PATTERN, self::PARAM_REPLACEMENT_PATTER, $key);
        $pattern = '/^' . preg_replace('/\//', '\/', $pattern) . '$/';

        $keyArr = explode(self::DELIMITER_ROUTER, $value);

        $this->key = $key;
        $this->rawData = $value;
        $this->pattern = $pattern;
        $this->controller = $keyArr[0] ?? '';
        $this->method = $keyArr[1] ?? '';
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->{$name})) {
            return $this->{$name};
        } else {
            throw new \RuntimeException ('Property ' . $name . ' does not exist');
        }
    }

    /**
     * @param string $method
     * @param string $uri
     */
    public function setParams(string $method, string $uri): void
    {
        if ($method == 'GET') {
            $paramKeys = [];
            $arrayKey = explode('/', $this->key);
            foreach ($arrayKey as $key => $item) {
                if (preg_match(self::PARAM_PATTERN, $item)) {
                    $paramName = str_replace(['{', '}'], '', $item);
                    $paramKeys[$paramName] = $key;
                }
            }

            $arrayUri = explode('/', $uri);

            foreach ($paramKeys as $paramName => $paramKey) {
                if (isset($arrayUri[$paramKey]) && !preg_match(self::PARAM_PATTERN, $arrayUri[$paramKey])) {
                    $this->params[$paramName] = $arrayUri[$paramKey];
                }
            }
        } elseif ($method == 'POST') {
            $this->params = $_POST;
        }

    }

}