<?php
namespace Tim\Core;

use Tim\Core\Contracts\Singleton;
use Tim\Core\Contracts\Request as IRequest;

class Request implements Singleton, IRequest
{
    /**
     * @var $this |Request|null
     */
    protected static ?self $instance = null;

    /**
     * @var string
     */
    public string $uri;

    /**
     * @var string
     */
    public string $method;

    /**
     * Request constructor.
     */
    protected function __construct()
    {
        self::$instance = $this;

        $this->method = $_SERVER['REQUEST_METHOD'] ?? '';

        $this->uri = $_SERVER['REQUEST_URI'] ?? '';
    }

    /**
     * @return static
     */
    public static function getInstance() : self
    {
        return self::$instance ?? new self;
    }
}