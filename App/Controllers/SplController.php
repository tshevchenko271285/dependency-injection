<?php
namespace Tim\App\Controllers;

use Tim\Core\App;
use Tim\Core\Application;
use Tim\Core\Contracts\Controller;
use Tim\Core\Launcher;
use Tim\Core\Request;

class SplController implements Controller
{
    public function TimSplDoublyLinkedList(Request $request, \SplDoublyLinkedList $list)
    {
        echo '<pre>';
        var_dump($request->routes);
        echo '<hr>';
        foreach ($request->routes as $route) {
            $list->push($route);
            echo 'push(' . $route->key  . ')' . PHP_EOL;
        }
        echo '<hr>';
        echo 'Default mode: ' . $list->getIteratorMode() . ' (IT_MODE_FIFO)' . PHP_EOL;
        foreach ($list as $key => $item) {
            echo '<hr>';
            var_dump($key, $item->key);
            echo '<hr>';
        }
        $list->setIteratorMode(\SplDoublyLinkedList::IT_MODE_LIFO);
        echo '<hr>';
        echo 'Set mode: ' . $list->getIteratorMode() . ' (IT_MODE_LIFO)' . PHP_EOL;

        foreach ($list as $key => $item) {
            echo '<hr>';
            var_dump($key, $item->key);
            echo '<hr>';
        }
        echo '<hr>';
    }

    public function TimSplStack(Request $request, \SplStack $stack)
    {
        echo '<pre>';
        var_dump($stack);
        echo '<pre>';
//        var_dump($request->routes);
        echo '<hr>';
        foreach ($request->routes as $route) {
            $stack->push($route);
            echo 'push(' . $route->key  . ')' . PHP_EOL;
        }
        echo '<hr>';
        foreach ($stack as $key => $item) {
            $line = $key . ' : ' . $item->key;
            if ($item === $stack->top()) {
                $line .= ' (Первый элемент)';
            }
            if ($item === $stack->bottom()) {
                $line .= ' (Последний элемент)';
            }
            $line .= PHP_EOL;
            echo $line;
        }
        echo '<hr>';
    }

    public function TimSplQueue(\SplQueue $queue)
    {
        $request = App::getInstance()->makeObject(Request::class);
        echo '<pre>';
        var_dump($queue);
        echo '<pre>';
        var_dump($request->routes);
        echo '<hr>';
        foreach ($request->routes as $route) {
            $queue->push($route);
            echo 'push(' . $route->key  . ')' . PHP_EOL;
        }
        echo '<hr>';
        foreach ($queue as $key => $item) {
            $line = $key . ' : ' . $item->key;
            if ($item === $queue->top()) {
                $line .= ' (Первый элемент)';
            }
            if ($item === $queue->bottom()) {
                $line .= ' (Последний элемент)';
            }
            $line .= PHP_EOL;
            echo $line;
        }
        echo '<hr>';
    }

    public function TimSplHeap(Request $request, \SplHeap $heap)
    {
        echo 'not extended';
//        echo '<pre>';
//        var_dump($heap);
//        echo '<pre>';
//        var_dump($app->routes);
//        echo '<hr>';
//        foreach ($app->routes as $key => $route) {
//            $heap->push($route);
//            echo '$heap->insert(' . $route  . ', '.$key.')' . PHP_EOL;
//        }
//        echo '<hr>';
//        foreach ($heap as $key => $item) {
//            $line = $key . ' : ' . $item;
//            if ($item === $heap->top()) {
//                $line .= ' (Первый элемент)';
//            }
//            $line .= PHP_EOL;
//            echo $line;
//        }
//        echo '<hr>';
    }
}
