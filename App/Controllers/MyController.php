<?php

namespace Tim\App\Controllers;

use Tim\App\Services\UserService;
use Tim\Core\App;
use Tim\Core\Contracts\Controller;
use Tim\Core\Contracts\Request;
use Tim\Core\Contracts\Routes;
use Tim\Core\View;

class MyController implements Controller
{
    public function home(Request $request)
    {

        $title = 'Hello World';
        $desc = 'Lorem ipsum dolor sit amet.';
        $app = App::getInstance();

        $contentData = [
            'title' => $title,
            'desc' => $desc,
            'route' => $app->route,
            'routes' => $app->routes,
        ];

        $view = View::render('_header', ['title' => $title]);
        $view .= View::render('example.content', $contentData);
        $view .= View::render('_footer');

        echo $view;
    }

    public function getPost()
    {
        var_dump(__METHOD__);
    }
    public function getArticle(App $app)
    {
        var_dump($app->route);
        var_dump(__METHOD__);
    }

    public function getRoutes(Request $request)
    {
        foreach ($request->routes as $key => $route) {
            var_dump($key, $route);
        }
        echo '<hr>';
    }
}