<?php
namespace Tim\App\Spl;

class TimSplHeap extends \SplHeap
{
    /**
     * Extracts a node from top of the heap and sift up.
     * @link https://php.net/manual/en/splheap.extract.php
     * @return mixed The value of the extracted node.
     */
    public function extract () {}

    /**
     * Inserts an element in the heap by sifting it up.
     * @link https://php.net/manual/en/splheap.insert.php
     * @param mixed $value <p>
     * The value to insert.
     * </p>
     * @return void
     */
    public function insert ($value) {}

    /**
     * Peeks at the node from the top of the heap
     * @link https://php.net/manual/en/splheap.top.php
     * @return mixed The value of the node on the top.
     */
    public function top () {}

    /**
     * Counts the number of elements in the heap.
     * @link https://php.net/manual/en/splheap.count.php
     * @return int the number of elements in the heap.
     */
    public function count () {}

    /**
     * Checks whether the heap is empty.
     * @link https://php.net/manual/en/splheap.isempty.php
     * @return bool whether the heap is empty.
     */
    public function isEmpty () {}

    /**
     * Rewind iterator back to the start (no-op)
     * @link https://php.net/manual/en/splheap.rewind.php
     * @return void
     */
    public function rewind () {}

    /**
     * Return current node pointed by the iterator
     * @link https://php.net/manual/en/splheap.current.php
     * @return mixed The current node value.
     */
    public function current () {}

    /**
     * Return current node index
     * @link https://php.net/manual/en/splheap.key.php
     * @return int The current node index.
     */
    public function key () {}

    /**
     * Move to the next node
     * @link https://php.net/manual/en/splheap.next.php
     * @return void
     */
    public function next () {}

    /**
     * Check whether the heap contains more nodes
     * @link https://php.net/manual/en/splheap.valid.php
     * @return bool true if the heap contains any more nodes, false otherwise.
     */
    public function valid () {}

    /**
     * Recover from the corrupted state and allow further actions on the heap.
     * @link https://php.net/manual/en/splheap.recoverfromcorruption.php
     * @return void
     */
    public function recoverFromCorruption () {}

    /**
     * Compare elements in order to place them correctly in the heap while sifting up.
     * @link https://php.net/manual/en/splheap.compare.php
     * @param mixed $value1 <p>
     * The value of the first node being compared.
     * </p>
     * @param mixed $value2 <p>
     * The value of the second node being compared.
     * </p>
     * @return int Result of the comparison, positive integer if <i>value1</i> is greater than <i>value2</i>, 0 if they are equal, negative integer otherwise.
     * </p>
     * <p>
     * Having multiple elements with the same value in a Heap is not recommended. They will end up in an arbitrary relative position.
     */
    protected function compare ($value1, $value2) {}

    /**
     * @return bool
     */
    public function isCorrupted(){}


    /**
     * @return array
     * @since 7.4
     */
    public function __debugInfo(){}

}