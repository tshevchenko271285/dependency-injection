<?php

namespace Tim\App\Services;

use Tim\Core\Contracts\Request;

class UserService
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getUrl()
    {
        return $this->request->uri;
    }
}