############
# BASIC COMMAND
############
# Add group
sudo usermod -a -G www-data dev
# Show groups
groups dev
# Change owner
sudo chown -R www-data:www-data your_path


############
# PhpStorm RESET
############
rm -rf ~/.config/JetBrains/PhpStorm*/eval
sed -i '/evlsprt/d' ~/.config/JetBrains/PhpStorm*/options/other.xml
rm -rf ~/.java/.userPrefs/jetbrains/phpstorm


# - Создание виртуальных хостов
sudo bash -c '
SITES_DIRECTORY="/home/dev/www/"
HOST_BEING_CREATED="hello-world.loc"
OWNER_USER="dev"
OWNER_GROUP="dev"


rm -rf $SITES_DIRECTORY$HOST_BEING_CREATED
mkdir $SITES_DIRECTORY$HOST_BEING_CREATED
chown $OWNER_USER:$OWNER_GROUP $SITES_DIRECTORY$HOST_BEING_CREATED

rm /etc/apache2/sites-available/$HOST_BEING_CREATED.conf
touch /etc/apache2/sites-available/$HOST_BEING_CREATED.conf

echo "<VirtualHost *:80>
    ServerName $HOST_BEING_CREATED
    ServerAlias www.$HOST_BEING_CREATED
    DocumentRoot $SITES_DIRECTORY$HOST_BEING_CREATED
    <Directory $SITES_DIRECTORY$HOST_BEING_CREATED>
        AllowOverride All
        Options -Indexes +FollowSymLinks +MultiViews
        Require all granted
    </Directory>
    ErrorLog $SITES_DIRECTORY$HOST_BEING_CREATED/error.log
    CustomLog $SITES_DIRECTORY$HOST_BEING_CREATED/access.log combined
</VirtualHost>
" >> /etc/apache2/sites-available/$HOST_BEING_CREATED.conf

echo "<h1>$HOST_BEING_CREATED</h1>" >> $SITES_DIRECTORY$HOST_BEING_CREATED/index.php
chown $OWNER_USER:$OWNER_GROUP $SITES_DIRECTORY$HOST_BEING_CREATED/index.php

sudo a2ensite $HOST_BEING_CREATED
sudo systemctl reload apache2

echo "127.0.0.1 $HOST_BEING_CREATED" >> /etc/hosts

google-chrome http://$HOST_BEING_CREATED --no-sandbox

unset HOST_BEING_CREATED
unset OWNER_USER
unset OWNER_GROUP
'
############
# HARDWARE INFO
############
sudo apt-get install lshw
sudo lshw

############
# CHANGE PHP CLI
############
sudo update-alternatives --set php /usr/bin/php5.6
sudo update-alternatives --config php

############
# XDEBUG
# xdebug_info();
############
sudo apt install php-xdebug
sudo nano /etc/php/8.0/apache2/php.ini
# STEP DEBUGGING
[xdebug]
zend_extension=/usr/lib/php/20200930/xdebug.so
xdebug.mode = debug

############
# FTP
############
sudo apt-get install proftpd
sudo nano /etc/proftpd/proftpd.conf
#TransferLog /var/log/proftpd/xferlog
#SystemLog   /var/log/proftpd/proftpd.log
#DefaultRoot ~
sudo systemctl enable|restart proftpd

############
# SSH KEY
############
# SERVER
sudo apt-get install ssh
# SETTINGS: /etc/ssh/sshd_config
PermitRootLogin no
PasswordAuthentication no|yes
PubkeyAuthentication yes
#sudo service ssh restart|stop|start
# CLIENT
ssh-keygen
ssh-copy-id username@host

############
# VirtualHost
############

# Common Name firstssl.loc
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-firstssl.key -out /etc/ssl/certs/apache-firstssl.crt

sudo nano /etc/apache2/sites-available/firstssl.loc.conf
sudo a2ensite firstssl.loc.conf
sudo systemctl reload apache2
sudo nano /etc/hosts # add 127.0.0.1 firstssl.loc

# HTTP firstssl.loc.conf
<VirtualHost *:80>
    ServerName firstssl.loc
    ServerAdmin webmaster@localhost
    DocumentRoot /home/dev/www/firstssl.loc
    <Directory /home/dev/www/firstssl.loc>
        AllowOverride All
        Options -Indexes +FollowSymLinks +MultiViews
        Require all granted
    </Directory>
    ErrorLog /home/dev/www/firstssl.loc/error.log
    CustomLog /home/dev/www/firstssl.loc/access.log combined
</VirtualHost>

# SSL firstssl.loc.conf
<VirtualHost *:443>
    ServerName firstssl.loc
    DocumentRoot /home/dev/www/firstssl.loc

    <Directory /home/dev/www/firstssl.loc>
        AllowOverride All
        Options -Indexes +FollowSymLinks +MultiViews
        Require all granted
    </Directory>

    SSLEngine on
    SSLCertificateFile /etc/ssl/certs/apache-firstssl.crt
    SSLCertificateKeyFile /etc/ssl/private/apache-firstssl.key

    ErrorLog /home/dev/www/firstssl.loc/error.log
    CustomLog /home/dev/www/firstssl.loc/access.log combined
</VirtualHost>
<VirtualHost *:80>
    ServerName firstssl.loc
    Redirect / https://firstssl.loc/
</VirtualHost>


#<VirtualHost *:443>
#    ServerName black-dev.loc
#    DocumentRoot /home/dev/www/black-dev.loc
#
#    <Directory /home/dev/www/black-dev.loc>
#        AllowOverride All
#        Options -Indexes +FollowSymLinks +MultiViews
#        Require all granted
#    </Directory>
#
#    SSLEngine on
#    SSLCertificateFile /etc/ssl/certs/black-dev-loc.crt
#    SSLCertificateKeyFile /etc/ssl/private/black-dev-loc.key
#
#    ErrorLog /home/dev/www/black-dev.loc/error.log
#    CustomLog /home/dev/www/black-dev.loc/access.log combined
#</VirtualHost>
#<VirtualHost *:80>
#    ServerName black-dev.loc
#    Redirect / https://black-dev.loc/
#</VirtualHost>
