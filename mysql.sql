-- Удалить базу
DROP DATABASE IF EXISTS `tim_loc`;
-- Создать базу
CREATE DATABASE IF NOT EXISTS `tim_loc`;
-- Показать доступные базы
SHOW DATABASES;
SHOW TABLES In tim_loc;
-- Выбрать базу
-- use tim_loc;


-- Таблица продуктов
DROP TABLE IF EXISTS tim_loc.products;
CREATE TABLE IF NOT EXISTS tim_loc.products (
     id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
     title VARCHAR(255) NOT NULL,
     price DECIMAL(5,2) NOT NULL,
     INDEX idxPriceTitle (title)
) CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=INNODB;

-- Таблица заказов
DROP TABLE IF EXISTS tim_loc.orders;
CREATE TABLE IF NOT EXISTS tim_loc.orders (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    status VARCHAR(255) NOT NULL,
    INDEX idxOrderStatus (status)
) CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=INNODB;

-- Таблица связей заказ / продукт
DROP TABLE IF EXISTS tim_loc.order_product;
CREATE TABLE IF NOT EXISTS tim_loc.order_product (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    order_id INT UNSIGNED NOT NULL,
    product_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (order_id)
        REFERENCES tim_loc.orders(id)
        ON DELETE CASCADE,
    FOREIGN KEY (product_id)
        REFERENCES tim_loc.products(id)
        ON DELETE RESTRICT,
    INDEX idxRelationOrder (order_id),
    INDEX idxRelationProduct (product_id)
) ENGINE=INNODB;



INSERT INTO tim_loc.products (title, price) VALUES ('Продукт 1', 10.55);
INSERT INTO tim_loc.products (title, price) VALUES ('Продукт 2', 14.10);
INSERT INTO tim_loc.products (title, price) VALUES ('Продукт 3', 5.20);
INSERT INTO tim_loc.products (title, price) VALUES ('Продукт 4', 10.99);
INSERT INTO tim_loc.products (title, price) VALUES ('Продукт 5', 8.99);

-- Работа индексов
EXPLAIN SELECT * FROM tim_loc.products WHERE title = 'Product2' \G
EXPLAIN SELECT * FROM tim_loc.products WHERE title LIKE '%Product2%' \G
ALTER TABLE tim_loc.products DROP INDEX idxPriceTitle;
EXPLAIN SELECT * FROM tim_loc.products WHERE title = 'Product2' \G
CREATE INDEX idxPriceTitle ON tim_loc.products(title);

-- Создание заказов
INSERT INTO tim_loc.orders (title, status)
VALUES('Заказ от Васи', 'NEW'), ('Заказ от Пети', 'NEW');

-- Добавление товаров в заказ
INSERT INTO tim_loc.order_product (order_id, product_id)
VALUES (1, 1), (1, 3), (1, 5), (2, 2), (2, 4);

-- Выбор всех товаров для заказа
SELECT tim_loc.products.title AS `Product Name`, tim_loc.products.price AS Price FROM tim_loc.orders
INNER JOIN tim_loc.order_product ON tim_loc.orders.id = tim_loc.order_product.order_id
INNER JOIN tim_loc.products ON tim_loc.order_product.product_id = tim_loc.products.id
WHERE tim_loc.order_product.order_id = 1;

-- Ссылочная целостность
SELECT * FROM tim_loc.order_product;
SELECT * FROM tim_loc.products;
DELETE FROM tim_loc.products WHERE tim_loc.products.id = 1;
DELETE FROM tim_loc.orders WHERE tim_loc.orders.id = 1;

-- Удалить таблицу
DROP TABLE IF EXISTS tim_loc.tasks;
-- Coздание таблицы
CREATE TABLE IF NOT EXISTS tim_loc.tasks (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(255) NOT NULL ,
    estimated TINYINT UNSIGNED NOT NULL,
	created DATETIME,
	updated DATETIME,
	INDEX idxTasksTitle (title)
) CHARACTER SET utf8 COLLATE utf8_general_ci;
-- Просмотр структуры таблицы
DESC tim_loc.tasks;
-- Показать доступные таблицы
SHOW TABLES IN tim_loc;


-- DROP TRIGGER
DROP TRIGGER IF EXISTS tim_loc.tasks_created_date;
-- CREATE TRIGGER
CREATE
    DEFINER = root@localhost
    TRIGGER tim_loc.tasks_created_date BEFORE INSERT ON tim_loc.tasks
    FOR EACH ROW SET NEW.created = NOW(), NEW.updated = NOW();

-- DROP TRIGGER
DROP TRIGGER IF EXISTS tim_loc.tasks_updated_date;
-- CREATE TRIGGER
CREATE
    DEFINER = root@localhost
    TRIGGER tim_loc.tasks_updated_date BEFORE UPDATE ON tim_loc.tasks
    FOR EACH ROW SET NEW.updated = NOW();

-- SHOW TRIGGERS
SHOW TRIGGERS IN tim_loc \G

INSERT INTO tim_loc.tasks (title, estimated)
    VALUES ('first task', 3), ('task two', 4), ('task three', 1);

SELECT * FROM tim_loc.tasks;
UPDATE tim_loc.tasks SET title = 'two title' WHERE id = 2;

-- CREATE VIEW
CREATE OR REPLACE
    DEFINER = root@localhost
    SQL SECURITY DEFINER -- DEFINER / INVOKER
    VIEW tim_loc.vtasks
    AS SELECT
        title,
        estimated,
        updated as start_time,
        DATE_ADD(updated, INTERVAL estimated HOUR) as dead_time
    FROM tim_loc.tasks;

SELECT * FROM tim_loc.tasks;
SELECT * FROM tim_loc.vtasks;


-- Показать пользователей
SELECT User FROM mysql.user;
-- Текущий пользователь
SELECT USER();
-- Удалить пользователя
DROP USER IF EXISTS tim@localhost;
-- Создать пользователя
CREATE USER tim@localhost IDENTIFIED BY '1111';

-- Определение прав пользователя
GRANT ALL PRIVILEGES ON tim_loc.* TO tim@localhost;
GRANT SELECT, UPDATE ON tim_loc.vtasks TO tim@localhost;
GRANT EXECUTE ON tim_loc.* TO tim@localhost;
-- GRANT INSERT ON tim_loc.vtasks TO tim@localhost;
-- REVOKE ALL PRIVILEGES ON tim_loc.* FROM tim@localhost;
-- REVOKE UPDATE ON tim_loc.* FROM tim@localhost;

-- Показать привелегии
SHOW GRANTS FOR CURRENT_USER;
SHOW GRANTS FOR tim@localhost;
-- Обновить привелегии
FLUSH PRIVILEGES;

-- Тест прав
SELECT * FROM tim_loc.tasks;
INSERT INTO tim_loc.tasks (title, estimated) VALUES ('first title', 3);
UPDATE tim_loc.tasks SET title = 'two title' WHERE id = 2;
-- DELETE FROM table WHERE id = 11;



-- Список хранимых процедур
SELECT routine_name, routine_definition, definer
FROM information_schema.routines
WHERE
      -- routine_name = 'set_created_date' AND
      routine_schema = 'tim_loc' \G


-- DROP PROCEDURE
DROP PROCEDURE IF EXISTS tim_loc.set_created_date;
-- CREATE PROCEDURE
delimiter //
CREATE
    DEFINER = root@localhost
	PROCEDURE tim_loc.set_created_date (IN tab_name VARCHAR(40), IN row_id INT)
    SQL SECURITY DEFINER -- DEFINER / INVOKER
	BEGIN
        SET @sql = CONCAT(
            'UPDATE ', tab_name,
            ' SET created = "', NOW(),
            '", updated = "', NOW(),
            '" WHERE id = ', row_id
        );
        PREPARE stmt3 FROM @sql;
        EXECUTE stmt3;
        DEALLOCATE PREPARE stmt3;
	END
	//
delimiter ;
-- Вызов процедуры
CALL tim_loc.set_created_date('tim_loc.tasks', 1);
UPDATE tim_loc.tasks SET updated = NULL WHERE id = 1;

-- DROP PROCEDURE
DROP PROCEDURE IF EXISTS tim_loc.set_updated_date;
-- CREATE PROCEDURE
delimiter //
CREATE
	PROCEDURE tim_loc.set_updated_date (IN tab_name VARCHAR(40), IN row_id INT)
	BEGIN
        SET @sql = CONCAT(
            'UPDATE ', tab_name,
            ' SET updated = "', NOW(),
            '" WHERE id = ', row_id
        );
        PREPARE stmt3 FROM @sql;
        EXECUTE stmt3;
        DEALLOCATE PREPARE stmt3;
	END
	//
delimiter ;

-- Вызов процедуры
CALL tim_loc.set_updated_date('tim_loc.tasks', 2);





-- Transactions
-- SET autocommit=0;
START TRANSACTION;
INSERT INTO tim_loc.tasks (title, estimated) VALUES ('Transaction', 3);
COMMIT; -- ROLLBACK;
