<?php

use PHPUnit\Framework\TestCase;
use Tim\Tests\Services\TestService;
use Tim\Tests\Services\TestServiceWithParam;

class ExampleTest extends TestCase
{
    public function testMock()
    {
        $mock = $this->createMock(TestService::class);

        $mock->expects($this->once())
            ->method('getData')
            ->with(['text' => 'Your Text']);

        $service = new TestServiceWithParam($mock);
        $service->getData('Your Text');
    }
}