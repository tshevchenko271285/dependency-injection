<?php

use \PHPUnit\Framework\TestCase;
use Tim\Core\Request;


class RequestTest extends TestCase
{
    protected $request;

    protected function setUp(): void
    {
        $this->request = Request::getInstance();;
    }

    public function testGetInstance()
    {
        $this->assertInstanceOf( 'Tim\Core\Contracts\Request', $this->request);
    }

    public function testFields()
    {
        $className = get_class($this->request);
        $this->assertClassHasStaticAttribute('instance', $className);
        $this->assertClassHasAttribute('method', $className);
        $this->assertClassHasAttribute('uri', $className);
    }
}