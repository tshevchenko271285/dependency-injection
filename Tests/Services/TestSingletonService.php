<?php


namespace Tim\Tests\Services;


use Tim\Core\Contracts\Singleton;

class TestSingletonService implements Singleton
{
    protected static ?self $instance = null;

    protected function __construct(){}

    protected function __clone() {}

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance() : self
    {
        if(!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }
}