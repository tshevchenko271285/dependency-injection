<?php
namespace Tim\Tests\Services;


class TestServiceWithParam
{
    protected TestService $service;

    public function __construct(TestService $service)
    {
        $this->service = $service;
    }

    public function getData($text): string
    {
        return $this->service->getData(['text' => $text]);
    }
}