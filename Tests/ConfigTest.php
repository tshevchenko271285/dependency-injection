<?php

use \PHPUnit\Framework\TestCase;
use \Tim\Core\Config;

class ConfigTest extends TestCase
{

    public function testGet()
    {
        $testValue = Config::get('test.data');
        $this->assertEquals('value', $testValue);
    }

    public function testGetDefault()
    {
        $testdefaultValue = Config::get('test.notExistData', 'defaultValue');
        $this->assertEquals('defaultValue', $testdefaultValue);
    }

}