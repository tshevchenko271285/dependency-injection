<?php

use \PHPUnit\Framework\TestCase;
use \Tim\Core\Route;

class RouteTest extends TestCase
{
    protected $route;

    protected $routeKey = '/post/{post_id}/article/{article_id}';

    protected $routeValue = 'MyController@getArticle';

    protected $routeUri = '/post/123/article/321';

    protected $requestMethod;

    protected function setUp(): void
    {
        $this->route = new Route($this->routeKey, $this->routeValue);
    }

    public function testProperties()
    {
        $this->assertEquals($this->routeKey, $this->route->key);
        $this->assertEquals($this->routeValue, $this->route->rawData);
        $this->assertEquals('MyController', $this->route->controller);
        $this->assertEquals('getArticle', $this->route->method);
        $this->assertRegExp($this->route->pattern, $this->routeUri);
    }

    public function testNotExistProperty()
    {
        $this->expectException(\RuntimeException::class);
        $this->route->notExistProperty;
    }

    public function testSetParams()
    {
        $this->assertEmpty($this->route->params);
        $this->route->setParams('GET', $this->routeUri);
        $this->assertEquals([123, 321], $this->route->params);
    }

    public function testPostParams()
    {
//        $this->assertEmpty($this->route->params);
        $_POST = ['test_data' => 'test_value'];
        $this->route->setParams('POST', $this->routeUri);
        $this->assertEquals($_POST, $this->route->params);
    }
}