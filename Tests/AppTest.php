<?php

use \PHPUnit\Framework\TestCase;
use Tim\Core\Contracts\Controller;
use Tim\Tests\Services\TestService;
use Tim\Tests\Services\TestServiceWithParam;
use Tim\Tests\Services\TestSingletonService;
use Tim\Core\App;

class AppTest extends TestCase
{
    protected App $app;

    protected function setUp(): void
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $_SERVER['REQUEST_URI'] = '/test/123/something/321';
        $this->app = App::getInstance();
    }

    public function testGetInstance()
    {
        $this->assertInstanceOf(App::class, $this->app);
        $this->assertClassHasStaticAttribute('instance', App::class);
        $this->assertClassHasAttribute('container', App::class);

        $this->assertInstanceOf(\Tim\Core\Contracts\Request::class, $this->app->request);
        $this->assertInstanceOf(\Tim\Core\Contracts\Route::class, $this->app->route);
    }

    public function testMakeObjectWithRelation()
    {
        $obj = $this->app->makeObject('testService');
        $this->assertInstanceOf(TestService::class, $obj);
    }

    public function testMakeObjectWithoutRelation()
    {
        $obj = $this->app->makeObject(TestService::class);
        $this->assertInstanceOf(TestService::class, $obj);
    }

    public function testMakeObjectWithParams()
    {
        $obj = $this->app->makeObject(TestServiceWithParam::class);
        $this->assertInstanceOf(TestServiceWithParam::class, $obj);
    }

    public function testMakeObjectSingleton()
    {
        $obj = $this->app->makeObject(TestSingletonService::class);
        $this->assertInstanceOf(TestSingletonService::class, $obj);
    }

    public function testMakeObjectException()
    {
        $this->expectException(\RuntimeException::class);
        $this->app->makeObject('\Tim\Core\NotExistClass');
    }

    public function testGetRelation()
    {
        $relation = $this->app->getRelation('testService');
        $this->assertEquals(TestService::class, $relation);

        $notExistRelation = $this->app->getRelation('testNotExistRelation');
        $this->assertEquals(null, $notExistRelation);
    }


    public function testRun()
    {
        $this->app->run();
        $this->assertInstanceOf(Controller::class, $this->app->currentController);
        $this->assertInstanceOf(\ReflectionMethod::class, $this->app->currentControllerMethod);
    }


    public function testRunException()
    {
        $this->expectException(\RuntimeException::class);
        $route = $this->getMockBuilder(\Tim\Core\Route::class)
                 ->setConstructorArgs(['not/exist/route', 'notExistController@notExistMethod'])
                 ->getMock();
        App::getInstance()->route = $route;
        App::getInstance()->run();
    }
}