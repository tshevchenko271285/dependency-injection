<?php

use PHPUnit\Framework\TestCase;
use Tim\Core\View;

class ViewTest extends TestCase
{

    public function testRender()
    {
        $string = View::render('viewByTest');
        $this->expectOutputString('view by test not change this text');
        echo $string;
    }

    public function testRenderException()
    {
        $this->expectException(\RuntimeException::class);
        View::render('viewNotExistFile');
    }
}