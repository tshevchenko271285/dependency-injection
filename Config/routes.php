<?php
return [
    '/' => 'MyController@home',
    '/routes' => 'MyController@getRoutes',
    '/post/{id}' => 'MyController@getPost',
    '/post/{post_id}/article/{article_id}' => 'MyController@getArticle',
    '/spl/TimSplDoublyLinkedList' => 'SplController@TimSplDoublyLinkedList',
    '/spl/TimSplStack' => 'SplController@TimSplStack',
    '/spl/TimSplQueue' => 'SplController@TimSplQueue',
    '/spl/TimSplHeap' => 'SplController@TimSplHeap',

    // Testing roure
    '/test/{id1}/something/{id2}' => 'TestController@testMethod',
];