<?php
return [
    'Tim\App\ITestA' => 'Tim\App\TestA',
    'Tim\Core\Contracts\Request' => 'Tim\Core\Request',
    'Tim\Core\Contracts\Routes' => 'Tim\Core\Routes',
    'SplDoublyLinkedList' => 'Tim\App\Spl\TimSplDoublyLinkedList',
    'SplStack' => 'Tim\App\Spl\TimSplStack',
    'SplHeap' => 'Tim\App\Spl\TimSplHeap',

    // Testing data
    'testService' => 'Tim\Tests\Services\TestService',
    'TestSingletonService' => 'Tim\App\Tests\TestSingletonService',
];