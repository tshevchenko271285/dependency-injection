<?php
spl_autoload_register(function ($class) {
    $prefix = 'Tim\\';

    $base_dir = __DIR__ . '/../';
    $len = strlen($prefix);

    $relative_class = substr($class, $len);

    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    if (file_exists($file)) {
        require $file;
    } else {
//        throw new \http\Exception\RuntimeException($class . ' not exist');
    }
});