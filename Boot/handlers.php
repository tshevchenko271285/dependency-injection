<?php

/**
 * Handling unknown errors
 */
set_error_handler(function ($severity, $message, $filename, $lineno) {
    echo '<hr>';
    echo '<pre>';
        echo 'Error: ' . $message . '<br />';
        echo 'File: ' . $filename . '<br />';
        echo 'Line: ' . $lineno;
    echo '</pre>';
    echo '<hr>';
});

/**
 * Handling unknown exceptions
 */
set_exception_handler(function($exception){
    var_dump($exception);
});